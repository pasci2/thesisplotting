#!/usr/bin/env python3

import os
import numpy as np
import matplotlib.pyplot as plt

from scipy.signal import argrelmax

def getAllSessionsDataDics(dirPath):

    listOfDics = []
    sessions = [dir for dir in os.listdir(dirPath) if os.path.isdir(dirPath + '/' + dir)]

    for sess in sessions:
        if "Session" in sess:
            sessionPath = dirPath + '/' + sess
            dict = getDataDict(sessionPath + '/dataLog.rlog')
            dict['path'] = sessionPath
            dict['session'] = sess
            listOfDics.append(dict)

    return listOfDics



def getDataDict(path):
    dict = {}
    global_name = ''

    try:
        with open(path) as f:
            for line in f:
                # print(line)
                if '//' in line:
                    name = line[3:-1]
                    if not name:
                        # print("empty line with //")
                        continue
                    else:
                        array = np.array([])
                        dict[name] = array
                        global_name = name

                else:
                    lineContent = []
                    for e in line.split(' '):
                        try:
                            a = float(e)
                            lineContent.append(a)
                        except:
                            pass

                    if lineContent:
                        if dict[global_name].size != 0:
                            dict[global_name] = np.vstack((dict[global_name], np.array(lineContent)))
                        else:
                            dict[global_name] = np.array(lineContent)
    except:
        pass

    return dict


def simple2Dplot(x, yList, name, savePath, xAxisLabel, yAxisLabel, dataLabelList=[]):

    font = {'size' : 16}
    plt.rc('font', **font)


    if dataLabelList and (len(dataLabelList) != len(yList)):
        print('PROVIDE ONE LABEL FOR EVERY DATA TO PLOT')

    dataCount = 0
    for y in yList:
        plt.plot(x[:-60], y[:-60], label=dataLabelList[dataCount], linewidth=1.5)
        # if dataCount == 1:
        #     plt.plot(x[:-60], y[:-60], label=dataLabelList[dataCount], linewidth=1.5, linestyle='-', color='r')
        # else:
        #     plt.plot(x[:-60], y[:-60], label=dataLabelList[dataCount], linewidth=0.8)
        dataCount += 1



    plt.grid(True)
    plt.title(name)
    plt.xlabel(xAxisLabel)
    plt.ylabel(yAxisLabel)
    plt.legend(loc='upper right')

    fig = plt.gcf() #current figure instance
    fig.set_size_inches(12.6, 7.8, forward=True)
    # fig.show()

    saveDir = savePath + '/plots'
    if not os.path.exists(saveDir):
        os.makedirs(saveDir)

    savePath = saveDir + '/' + name + '.png'

    fig.savefig(savePath, dpi=300, facecolor='w', edgecolor='w',
            orientation='portrait', papertype=None, format='png',
            transparent=False, bbox_inches='tight', pad_inches=0.05, #padding if bbox is tight
            frameon=None)
    fig.clear()

def simple2DplotWithAverage(x, yList, name, savePath, xAxisLabel, yAxisLabel, dataLabelList=[]):

    font = {'size' : 16}
    plt.rc('font', **font)


    if dataLabelList and (len(dataLabelList) != len(yList)):
        print('PROVIDE ONE LABEL FOR EVERY DATA TO PLOT')

    dataCount = 0
    for y in yList[:-1]:
        plt.plot(x, y, label=dataLabelList[dataCount], linewidth=.8)
        dataCount += 1

    plt.plot(x, yList[-1], label=dataLabelList[dataCount], linewidth=1.5, color='k')
    dataCount += 1

    plt.grid(True)
    plt.title(name)
    plt.xlabel(xAxisLabel)
    plt.ylabel(yAxisLabel)
    plt.legend()

    fig = plt.gcf()  # current figure instance

    fig.set_size_inches(12.6, 7.8, forward=True)
    # fig.show()

    saveDir = savePath + '/plots'
    if not os.path.exists(saveDir):
        os.makedirs(saveDir)

    savePath = saveDir + '/' + name + '.png'

    fig.savefig(savePath, dpi=300, facecolor='w', edgecolor='w',
                orientation='portrait', papertype=None, format='png',
                transparent=False, bbox_inches='tight', pad_inches=0.05,  # padding if bbox is tight
                frameon=None)
    fig.clear()




def simple2yAxisPlot(x, y1, y2, name, savePath, xAxisLabel, yAxisLabel1, yAxisLabel2, dataLabel1, dataLabel2):

    font = {'size' : 16}
    plt.rc('font', **font)


    fig, ax1 = plt.subplots()
    ax1.set_ylabel(yAxisLabel1)
    ln1 = ax1.plot(x, y1, label=dataLabel1, color='b', linewidth=.8)

    ax2 = ax1.twinx()
    ax2.set_ylabel(yAxisLabel2)
    ln2 = ax2.plot(x, y2, label=dataLabel2, color='g', linewidth=.8)

    fig.tight_layout()

    plt.title(name)
    ax1.set_xlabel(xAxisLabel)
    # plt.xlabel(xAxisLabel)

    lns = ln1 + ln2
    labs = [l.get_label() for l in lns]
    ax1.legend(lns, labs, loc='upper left')

    fig = plt.gcf() #current figure instance

    fig.set_size_inches(12.6, 7.8, forward=True)
    # fig.show()

    saveDir = savePath + '/plots'
    if not os.path.exists(saveDir):
        os.makedirs(saveDir)

    savePath = saveDir + '/' + name + '.png'

    fig.savefig(savePath, dpi=300, facecolor='w', edgecolor='w',
            orientation='portrait', papertype=None, format='png',
            transparent=False, bbox_inches='tight', pad_inches=0.05, #padding if bbox is tight
            frameon=None)
    fig.clear()

def plotLearningRate(data, savePath):
    if data.shape[1] > 2:
        simple2Dplot(data[:, 1], [data[:, 2]], 'learningRate', savePath, 'environment Steps', 'learning Rate', ['learning Rate'])
    else:
        simple2Dplot(data[:, 0], [data[:, 1]], 'learningRate', savePath, 'environment Steps', 'learning Rate',
                     ['learning Rate'])

def plotStdev(data, savePath):

    firstStepEntry = data[0, 0]
    step = 0
    while data[step, 0] == firstStepEntry:
        step += 1

    simple2Dplot(data[0:-1:step, 0], [ data[0:-1:step, 2], data[0:-1:step, 3] ],
                 'stdev', savePath, 'environment Steps', 'stdev', ['stdev HFE', 'stdev KFE'])

def plotGradNorm(data, savePath):

    firstStepEntry = data[0, 0]
    step = 0
    while data[step, 0] == firstStepEntry:
        step += 1

    # simple2Dplot(data[0:-1:step, 0], [ data[0:-1:step, 2], data[0:-1:step, 3] ],
    #              'gradient Norm', savePath, 'environmentSteps', 'gradNorm', ['unclipped', 'clipped'])

    simple2Dplot(data[:, 0], [ data[:, 2], data[:, 3] ],
                 'gradient_Norm', savePath, 'update steps', 'gradNorm', ['unclipped', 'clipped'])


def plotAlgoData(data, savePath):

    # print(data[:, 0].shape)
    N = 500

    # firstStepEntry = data[0, 0]
    # step = 0
    # while data[step, 0] == firstStepEntry:
    #     step += 1

    # dataList = [data[:, 2], data[:, 3], data[:, 4], data[:, 5], -data[:, 6], data[:, 7]]
    #
    # legendList = [ 'kl', 'kl loss', 'ppo loss', 'vf loss', 'entropy loss', 'total loss']
    # if data.shape[1] > 8:
    #     # dataList.appennd(data[0:-1:step, 8])
    #     dataList.append(data[:, 8])
    #     legendList.append('dist loss')

    # simple2Dplot(data[0:-1:step, 0], dataList, 'learning algo data', savePath, 'environmentSteps', ' ', legendList)
    # simple2Dplot(data[:, 0], dataList, 'learning_algo_data', savePath, 'parameter updates', ' ', legendList)

    kl_filt = np.convolve(data[:, 2], np.ones(N)/N, mode='same')
    simple2Dplot(data[:, 0], [data[:, 2], kl_filt], 'kl_divergence', savePath, 'parameter updates', ' ', ['kl', 'kl filtered'])

    kl_loss_filt = np.convolve(data[:, 3], np.ones(N)/N, mode='same')
    simple2Dplot(data[:, 0], [data[:, 3], kl_loss_filt], 'kl_divergence_loss', savePath, 'parameter updates', ' ', ['kl loss', 'kl loss filtered'])

    ppo_loss_filt = np.convolve(data[:, 4], np.ones(N)/N, mode='same')
    simple2Dplot(data[:, 0], [data[:, 4], ppo_loss_filt], 'ppo_loss', savePath, 'parameter updates', ' ', ['ppo loss', 'ppo loss filtered'])

    vf_loss_filt = np.convolve(data[:, 5], np.ones(N)/N, mode='same')
    simple2Dplot(data[:, 0], [data[:, 5], vf_loss_filt], 'value_loss', savePath, 'parameter updates', ' ', ['vf loss', 'vf loss filtered'])

    entropy_loss_filt = np.convolve(data[:, 6], np.ones(N)/N, mode='same')
    simple2Dplot(data[:, 0], [-data[:, 6], -entropy_loss_filt], 'entropy_loss', savePath, 'parameter updates', ' ', ['entropy loss', 'entropy loss filtered'])

    total_loss_filt = np.convolve(data[:, 7], np.ones(N)/N, mode='same')
    simple2Dplot(data[:, 0], [data[:, 7], total_loss_filt], 'total_loss', savePath, 'parameter updates', ' ', ['total loss', 'total loss filtered'])

    if data.shape[1] > 8:
        dist_loss_filt = np.convolve(data[:, 8], np.ones(N)/N, mode='same')
        simple2Dplot(data[:, 0], [data[:, 8], dist_loss_filt], 'dist_loss', savePath, 'parameter updates', ' ', ['dist loss', 'dist loss filtered'])


def plotPerformance(data, savePath):
    filteredPerformance = np.convolve(data[:, 1], np.ones(50)/50, mode='same')

    # print(data[:,1].shape)

    np.save(savePath + '/filteredperformance', filteredPerformance)
    np.save(savePath + '/envSteps', data[:, 0])

    simple2Dplot(data[:, 0], [data[:, 1], filteredPerformance], 'learning performance', savePath, 'environment steps', 'cost per second', ['cost', 'filtered cost'])

    simple2Dplot(data[:, 0], [data[:, 2]], 'averaged_episode_length', savePath, 'environment steps', '[s]', ['episode length'])

def plotAvgCost(data, savePath):
    filteredCost = np.convolve(data[:, 1], np.ones(50)/50, mode='same')

    simple2Dplot(data[:, 0], [data[:, 1], filteredCost], 'learning performance', savePath, 'environment steps', 'cost per second', ['cost', 'filtered cost'])


def plotAdaptiveKlCoeff(data, savePath):
    dataSize = data.shape[0]

    simple2Dplot(range(0, dataSize), [data[:,1]], 'adaptive_kl_coeff', savePath, 'update steps', 'kl coeff', ['kl coeff'])



def plotCaplerlegState(data, savePath):
    # base height
    simple2yAxisPlot(data[:, 0] * 0.01, data[:, 2], data[:, 5], 'base', savePath, 'time [s]', 'base height [m]', 'base vel [m/s]', 'height', 'vel')

    simple2yAxisPlot(data[:, 0] * 0.01, data[:, 3], data[:, 6], 'hip', savePath, 'time [s]', 'hip pos [rad]', 'hip vel [rad/s]','pos', 'vel')

    simple2yAxisPlot(data[:, 0] * 0.01, data[:, 4], data[:, 7], 'knee', savePath, 'time [s]', 'knee pos [rad]', 'knee vel [rad/s]', 'pos', 'vel')


def plotCaplerlegAction(data, savePath):
    simple2Dplot(data[:, 0] * 0.01, [data[:, 2], data[:, 3]], 'hip_torque', savePath, 'time [s]', 'torque [Nm]', ['applied total action', 'action noise'])

    simple2Dplot(data[:, 0] * 0.01, [data[:, 4], data[:, 5]], 'knee_torque', savePath, 'time [s]', 'torque [Nm]', ['applied total action', 'action noise'])

def plotCaplerlegCost(data, savePath):

    mean = np.ones(data[:,0].shape) * np.mean(data[:,1])

    simple2Dplot(data[:, 0], [data[:, 1], mean], 'cost', savePath, 'sim steps', 'cost', ['cost', 'average cost'])


def plotData(saveDirPath, loggerFileName):

    path = saveDirPath + '/' + loggerFileName
    dict = getDataDict(path)

    for key in dict.keys():
        if dict[key].shape[0] < 40:
            pass
        elif "learningRate" in key:
            plotLearningRate(dict[key], saveDirPath)

        elif "taskStdev" in key:
            plotStdev(dict[key], saveDirPath)

        elif "gradNorm" in key:
            plotGradNorm(dict[key], saveDirPath)

        elif "algoData" in key:
            plotAlgoData(dict[key], saveDirPath)

        elif "performance" in key:
            plotPerformance(dict[key], saveDirPath)

        #old format
        elif "avgCost" in key:
            plotAvgCost(dict[key], saveDirPath)

        elif "adaptiveKlCoeff" in key:
            plotAdaptiveKlCoeff(dict[key], saveDirPath)

        elif "caplerlegState" in key:
            plotCaplerlegState(dict[key], saveDirPath)

        elif "caplerlegAction" in key:
            plotCaplerlegAction(dict[key], saveDirPath)

        elif "cost" in key:
            plotCaplerlegCost(dict[key], saveDirPath)

        else:
            print("NO FUNCTION TO PLOT ", key)

def plotData2(saveDirPath, loggerFileName, i):

    path = saveDirPath + '/' + loggerFileName
    dict = getDataDict(path)

    saveDirPath = saveDirPath + '/' + str(i) +  '/'

    for key in dict.keys():

        if dict[key].shape[0] < 50:
                pass
        elif "learningRate" in key:
            plotLearningRate(dict[key], saveDirPath)

        elif "taskStdev" in key:
            plotStdev(dict[key], saveDirPath)

        elif "gradNorm" in key:
            plotGradNorm(dict[key], saveDirPath)

        elif "algoData" in key:
            plotAlgoData(dict[key], saveDirPath)

        elif "performance" in key:
            plotPerformance(dict[key], saveDirPath)

        #old format
        elif "avgCost" in key:
            plotAvgCost(dict[key], saveDirPath)

        elif "adaptiveKlCoeff" in key:
            plotAdaptiveKlCoeff(dict[key], saveDirPath)

        elif "caplerlegState" in key:
            plotCaplerlegState(dict[key], saveDirPath)

        elif "caplerlegAction" in key:
            plotCaplerlegAction(dict[key], saveDirPath)

        elif "cost" in key:
            plotCaplerlegCost(dict[key], saveDirPath)

        else:
            print("NO FUNCTION TO PLOT ", key)

def plotDataFromDictWithPath(dict):

    saveDirPath = dict['path']

    for key in dict.keys():

        if dict[key].shape[0] < 40:
            pass
        elif "learningRate" in key:
            plotLearningRate(dict[key], saveDirPath)

        elif "taskStdev" in key:
            plotStdev(dict[key], saveDirPath)

        elif "gradNorm" in key:
            plotGradNorm(dict[key], saveDirPath)

        elif "algoData" in key:
            plotAlgoData(dict[key], saveDirPath)

        elif "performance" in key:
            plotPerformance(dict[key], saveDirPath)

        #old format
        elif "avgCost" in key:
            plotAvgCost(dict[key], saveDirPath)

        elif "adaptiveKlCoeff" in key:
            plotAdaptiveKlCoeff(dict[key], saveDirPath)

        elif "caplerlegState" in key:
            plotCaplerlegState(dict[key], saveDirPath)

        elif "caplerlegAction" in key:
            plotCaplerlegAction(dict[key], saveDirPath)

        elif "cost" in key:
            plotCaplerlegCost(dict[key], saveDirPath)

        else:
            print("NO FUNCTION TO PLOT ", key)

def plotSessionPerformances(listOfDicts, savePath):
    dataList = []
    filteredDataList = []
    plotNames = []

    for dict in listOfDicts:
        print(dict.keys())
        sessionName = dict['session']
        plotNames.append(sessionName)
        try:
            data = dict[sessionName + '/performance']
        except:
            continue

        dataList.append(data)
        filteredDataList.append(np.convolve(data[:, 1], np.ones(50)/50, mode='same'))


    total = np.zeros(filteredDataList[0].shape)

    for d in filteredDataList:
        total += d

    # print(total.shape)
    average = total/len(filteredDataList)
    np.save(savePath + '/averagedPerformance', average)
    np.save(savePath + '/envSteps', data[:,0])

    filteredDataList.append(average)
    plotNames.append('average')

    cropped = []
    for plot in filteredDataList:
        cropped.append(plot[:-60])


    # print(len(plotNames))
    # print(len(filteredDataList))
    simple2DplotWithAverage(data[:, 0][:-60], cropped, 'learning performance', savePath, 'environment steps',
                     'cost per second', plotNames)





def main():
    # old format runs 0-9

    # for i in range(24, 27):
    #     dirPath = '/home/pasci/.rai/logStorage/FINAL/' + str(i)
    #
    #     sessions = [dir for dir in os.listdir(dirPath) if os.path.isdir(dirPath + '/' + dir)]
    #
    #     sessionCount = 0
    #     for sess in sessions:
    #         sessionPath = dirPath + '/' + sess
    #
    #         plotData(sessionPath, 'dataLog.rlog')
    #         print('done with folder ' + str(i) + ' session ' + str(sessionCount))
    #         sessionCount += 1
    #
    #         runs = [dir for dir in os.listdir(sessionPath) if (os.path.isdir(sessionPath + '/' + dir) and "2018" in dir)]
    #         for run in runs:
    #             runPath = sessionPath + '/' + run
    #             plotData(runPath, 'dataLog.rlog')
    #


    # dirPath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/randInitPureModel/0.5/2018-07-31-16-18-06/'
    # sessions = [dir for dir in os.listdir(dirPath) if os.path.isdir(dirPath + '/' + dir)]
    #
    # for sess in sessions:
    #     sessionPath = dirPath + '/' + sess
    #     plotData(sessionPath, 'dataLog.rlog')
    #
    #     runs = [dir for dir in os.listdir(sessionPath) if (os.path.isdir(sessionPath + '/' + dir) and "2018" in dir)]
    #     for run in runs:
    #         runPath = sessionPath + '/' + run
    #         plotData(runPath, 'dataLog.rlog')


    # for i in [0, 20, 40, 60, 80, 100, 160, 200, 240, 299]:
    #     sessionPath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/randInitPureModel/0.5/2018-07-31-16-18-06/Session6/onlineLearning/noModel/noModel_costLog/' + str(i) + '/runNoNoise';
    #     plotData(sessionPath, 'dataLog.rlog')

    # sessionPath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/sameRandomForParallelInstances/0.0/Session8'
    # runs = [dir for dir in os.listdir(sessionPath) if (os.path.isdir(sessionPath + '/' + dir) and "2018" in dir)]
    # for run in runs:
    #     runPath = sessionPath + '/' + run
    #     plotData(runPath, 'dataLog.rlog')





    # dirPath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/sameRandomForParallelInstances/0.0/'
    # dirPath = '/home/pasci/.rai/logStorage/climber2/further_augmented_input_state/working_same_runs/2000steps/'
    # dirPath = '/home/pasci/.rai/logStorage/climber2/further_augmented_input_state/working_same_runs/2000steps/Session0'
    # plotData(dirPath, 'dataLog.rlog')

    # listOfDics = getAllSessionsDataDics(dirPath)

    # plotSessionPerformances(listOfDics, dirPath)




    # for dict in listOfDics:
    #     plotDataFromDictWithPath(dict)

    # for i in [0, 20, 40, 60, 80, 100, 160, 200, 240, 299]:
    #     plotData2(
    #         '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/randInitPureModel/0.5/2018-07-31-16-18-06/Session6/2018-08-06-10-40-16',
    #         'dataLog' + str(i) + '.rlog',
    #     i)



###
    # PLOT MODEL LEARNING DATA
###

    # dictList = []
    # graphNameList = []
    # sessionBasePath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/randInitPureModel/0.5/2018-07-31-16-18-06/Session6/onlineLearning/noModel/2000Steps_8Mini_10ep/seed_0_0/'
    # for i in [0, 12, 24, 36, 60, 120, 240]: #, 40, 60, 80, 100, 160, 200, 240, 299]:
    #     sessionPath = sessionBasePath + str(i) + '/runNoNoise'
    #     loggerFilePath = sessionPath + '/dataLog.rlog'
    #     dictList.append(getDataDict(loggerFilePath))
    #     graphNameList.append(str(int(20 * i / 60)) +  ' min')
    #
    #
    # caplerStateList = []
    # for dict in dictList:
    #     for k in dict.keys():
    #         if 'caplerlegState' in k:
    #             caplerStateList.append(dict[k])
    #
    # baseHeigtList = []
    # for data in caplerStateList:
    #     baseHeigtList.append(data[:,2])
    #
    # savePath = sessionBasePath
    # simple2Dplot(caplerStateList[0][:,0] * 0.01, baseHeigtList, 'online policy improvement', savePath, 'time [s]', 'base height [m]', graphNameList)




# ##
#     PLOT MODEL AND REAL STATE AND ACTION
# #
#     dirPath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/randInitPureModel/0.5/2018-07-31-16-18-06/Session6/onlineLearning/modelMPL_multipleTimesteps/modelTrainingDataMultipleStepsFINAL/1000_1000_8mini_10ep/seed_3_333/2018-08-21-17-05-09/'
#
#     dict = getDataDict(dirPath + 'dataLog.rlog')
#
#     stateData = []
#     costData = []
#     for k in dict.keys():
#         if 'caplerlegState' in k:
#             stateData.append(dict[k])
#
#         if 'cost' in k:
#             costData.append(dict[k])
#
#
#     for k in dict.keys():
#         if 'modelState' in k:
#             stateData.append(dict[k])
#
#         if 'modelCost' in k:
#             costData.append(dict[k])
#
#     print(costData[0].shape)
#     simple2Dplot(stateData[0][:,0] * 0.01, [stateData[0][:,2], stateData[1][:,2] ], 'base height', dirPath, 'time [s]', 'base height [m]', ['original simulation', 'improved model'])
#
#     simple2Dplot(costData[0][:,0] * 0.01, [costData[0][:,1], costData[1][:,1]/200.0 ], 'cost', dirPath, 'time [s]', 'cost', ['original simulation', 'improved model'])

###
# PLOT MODEL AND REAL STATE AND ACTION BAR PLOZ
##

    # allAverages = []
    # # basePath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/randInitPureModel/0.5/2018-07-31-16-18-06/Session6/onlineLearning/modelMPL_multipleTimesteps/modelTrainingDataMultipleStepsFINAL/1500_500_8mini_10ep/'
    # # basePath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/randInitPureModel/0.5/2018-07-31-16-18-06/Session6/onlineLearning/modelMPL_multipleTimesteps/modelTrainingDataMultipleStepsFINAL/1000_1000_8mini_10ep/'
    # basePath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/randInitPureModel/0.5/2018-07-31-16-18-06/Session6/onlineLearning/noModel/2000Steps_8Mini_10ep/'
    # font = {'size' : 16}
    # plt.rc('font', **font)
    #
    # for seed in ["0_0", "9_999", "11_111", "22_222", "987_777", "3_333", "123_456", "555_5"]:
    #
    #     dictList = []
    #     graphNameList = []
    #     for i in [0, 12, 24, 36, 60, 120, 240]: #, 40, 60, 80, 100, 160, 200, 240, 299]:
    #         sessionPath = basePath + 'seed_' + seed + '/' + str(i) + '/runNoNoise'
    #         loggerFilePath = sessionPath + '/dataLog.rlog'
    #         dictList.append(getDataDict(loggerFilePath))
    #         graphNameList.append(str(int(5 * i / 60)) +  ' min')
    #
    #
    #     caplerStateList = []
    #     for dict in dictList:
    #         for k in dict.keys():
    #             if 'caplerlegState' in k:
    #                 caplerStateList.append(dict[k])
    #
    #     baseHeigtList = []
    #     for data in caplerStateList:
    #         baseHeigtList.append(data[:,2])
    #
    #     hightAverages = []
    #     for baseH in baseHeigtList:
    #         extremaIdx = argrelmax(baseH)[0] #only one dimensional
    #         # print(baseH[extremaIdx])
    #         peaks = np.delete(baseH[extremaIdx],0,0) #remove initial hight
    #         hightAverages.append(np.average(peaks))
    #         # print(peaks)
    #     print(hightAverages)
    #
    #     allAverages.append(hightAverages)
    #
    # # print(allAverages)
    #
    # means = np.average(allAverages, axis=0)
    #
    # print(means)
    # print(means.shape[0]-1)
    # index = np.arange(means.shape[0])
    # print(index)
    # bar_width = 0.5
    # plt.bar(index, means, bar_width)
    # # plt.show()
    #
    #
    # savePath = basePath
    # # simple2Dplot(caplerStateList[0][:,0] * 0.01, baseHeigtList, 'online policy improvement', savePath, 'time [s]', 'base height [m]', graphNameList)
    #
    # name = "average hopping heights (8 seeds)"
    #
    # xAxisLabel = "real time data acquisition [min]"
    # yAxisLabel = "base height [m]"
    #
    #
    # plt.grid(True)
    # plt.title(name)
    # plt.xlabel(xAxisLabel)
    # plt.ylabel(yAxisLabel)
    # plt.ylim(0.7, 0.9)
    # ax = plt.gca() # gcf() #current figure instance
    #
    # fig = plt.gcf()
    # ax.set_xticks(index + bar_width / 2)
    # ax.set_xticklabels(('0', '4', '8', '12', '20', '40', '80'))
    # # plt.legend(loc='lower right')
    #
    #
    #
    # fig.set_size_inches(12.6, 7.8, forward=True)
    # # fig.show()
    #
    # saveDir = savePath + '/plots'
    # if not os.path.exists(saveDir):
    #     os.makedirs(saveDir)
    #
    # savePath = saveDir + '/' + name + '.png'
    #
    # fig.savefig(savePath, dpi=300, facecolor='w', edgecolor='w',
    #         orientation='portrait', papertype=None, format='png',
    #         transparent=False, bbox_inches='tight', pad_inches=0.05, #padding if bbox is tight
    #         frameon=None)
    # fig.clear()
    # #############################################################
    # ##
    # STDEV BAR PLOT
    # #

    # allAverages = []
    # # basePath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/randInitPureModel/0.5/2018-07-31-16-18-06/Session6/onlineLearning/modelMPL_multipleTimesteps/modelTrainingDataMultipleStepsFINAL/1500_500_8mini_10ep/'
    # # basePath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/randInitPureModel/0.5/2018-07-31-16-18-06/Session6/onlineLearning/modelMPL_multipleTimesteps/modelTrainingDataMultipleStepsFINAL/1000_1000_8mini_10ep/'
    # basePath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/randInitPureModel/0.5/2018-07-31-16-18-06/Session6/onlineLearning/noModel/2000Steps_8Mini_10ep/'
    # font = {'size' : 16}
    # plt.rc('font', **font)
    #
    # for seed in ["0_0", "9_999", "11_111", "22_222", "987_777", "3_333", "123_456", "555_5"]:
    #
    #     dictList = []
    #     graphNameList = []
    #     for i in [0, 12, 24, 36, 60, 120, 240]: #, 40, 60, 80, 100, 160, 200, 240, 299]:
    #         sessionPath = basePath + 'seed_' + seed + '/' + 'dataLog'+ str(i) + '.rlog'
    #         # loggerFilePath = sessionPath + '/dataLog.rlog'
    #         dictList.append(getDataDict(sessionPath))
    #         graphNameList.append(str(int(5 * i / 60)) +  ' min')
    #
    #
    #     caplerStateList = []
    #     for dict in dictList:
    #         for k in dict.keys():
    #             if 'taskStdev' in k:
    #                 caplerStateList.append(dict[k])
    #
    #     stdevNorms = []
    #     for data in caplerStateList:
    #         knee = np.array(data[:,2])
    #         hip = np.array(data[:, 3])
    #         # print (knee.shape)
    #
    #         concat = np.array([knee, hip])
    #         # print(concat.shape)
    #         norm = np.linalg.norm(concat.transpose(), axis=1)
    #         # print(norm.shape)
    #         stdevNorms.append(10.0*norm)
    #
    #     normAverages = []
    #     for stdev in stdevNorms:
    #         normAverages.append(np.average(stdev))
    #         # print(peaks)
    #     print(normAverages)
    #
    #     allAverages.append(normAverages)
    #
    # print(allAverages)
    #
    # means = np.average(allAverages, axis=0)
    #
    # print(means)
    # print(means.shape[0]-1)
    # index = np.arange(means.shape[0])
    # print(index)
    # bar_width = 0.5
    # plt.bar(index, means, bar_width)
    # # plt.show()
    #
    #
    # savePath = basePath
    # # simple2Dplot(caplerStateList[0][:,0] * 0.01, baseHeigtList, 'online policy improvement', savePath, 'time [s]', 'base height [m]', graphNameList)
    #
    # name = "Noise Standard Deviation Norm (8 seeds)"
    #
    # xAxisLabel = "real time data acquisition [min]"
    # yAxisLabel = "stdev norm [Nm]"
    #
    #
    # plt.grid(True)
    # plt.title(name)
    # plt.xlabel(xAxisLabel)
    # plt.ylabel(yAxisLabel)
    # plt.ylim(0.0, 16)
    # ax = plt.gca() # gcf() #current figure instance
    #
    # fig = plt.gcf()
    # ax.set_xticks(index + bar_width / 2)
    # # ax.set_xticklabels(('0', '2', '4', '6', '10', '20', '40'))
    # ax.set_xticklabels(('0', '4', '8', '12', '20', '40', '80'))
    # # plt.legend(loc='lower right')
    #
    #
    #
    # fig.set_size_inches(12.6, 7.8, forward=True)
    # # fig.show()
    #
    # saveDir = savePath + '/plots'
    # if not os.path.exists(saveDir):
    #     os.makedirs(saveDir)
    #
    # savePath = saveDir + '/' + name + '.png'
    #
    # fig.savefig(savePath, dpi=300, facecolor='w', edgecolor='w',
    #         orientation='portrait', papertype=None, format='png',
    #         transparent=False, bbox_inches='tight', pad_inches=0.05, #padding if bbox is tight
    #         frameon=None)
    # fig.clear()
    # #










    # if dataLabelList and (len(dataLabelList) != len(yList)):
    #     print('PROVIDE ONE LABEL FOR EVERY DATA TO PLOT')
    #
    # dataCount = 0
    # for y in yList:
    #     if dataCount == 6:
    #         plt.plot(x, y, label=dataLabelList[dataCount], linewidth=2.0)
    #     else:
    #         plt.plot(x, y, label=dataLabelList[dataCount], linewidth=.8)
    #     dataCount += 1
    #



    basePath = '/home/pasci/.rai/logStorage/targetHopping/fixedRandom/sameRandomForParallelInstances/'

    runs = ['0.0', '0.2', '0.3', '0.5', '0.8', '1.0']
    averagedPerformances = []
    plotNames = []
    for r in runs:
        averagedPerformances.append(np.load(basePath + r + '/averagedPerformance.npy'))
        plotNames.append(str(float(r)*100) + ' %')

    # print (averagedPerformances[0].shape[0])
    x = np.arange(averagedPerformances[0].shape[0])
    simple2Dplot(x*6000, averagedPerformances, 'learning performance (3000-3000 steps, 10 seeds)', basePath, 'total environment steps (real and artificial)',
                 'cost', plotNames)
    #
    #
    #
    #
    #
    #

    # i2aPath = '/home/pasci/.rai/logStorage/climber2/I2A/10steps_parallel/Session1/'
    # ppoPath = '/home/pasci/.rai/logStorage/climber2/further_augmented_input_state/working_same_runs/1000steps/'
    #
    # averagedPerformances = []
    # plotNames = []
    # averagedPerformances.append(np.load(i2aPath + 'filteredperformance.npy'))
    # plotNames.append('i2a')
    #
    # for i in [0,1,2]:
    #     dict = getDataDict(ppoPath + '/Session' + str(i)  + '/dataLog.rlog')
    #     for key in dict.keys():
    #         if 'performance' in key:
    #             data = dict[key]
    #             averagedPerformances.append(np.convolve(data[:, 1], np.ones(50) / 50, mode='same'))
    #             plotNames.append('no i2a, session' + str(i))
    #
    #
    # averagedPerformances.append(np.load(ppoPath + 'averagedPerformance.npy'))
    #
    # plotNames.append('no i2a, average')
    #
    # cropped = []
    # for plot in averagedPerformances:
    #     cropped.append(plot[:-60])
    #
    # # # print (averagedPerformances[0].shape[0])
    # # x = np.arange(averagedPerformances[0].shape[0])
    # x = np.load(i2aPath + 'envSteps.npy')
    # simple2Dplot(x[:-60], cropped, 'time_averaged_cost', i2aPath, 'environment steps', 'cost', plotNames)

if __name__ == '__main__':
    main()