import numpy as np

def main():
    a = np.array([1, 1, 3, 3, 1, 2])
    # N = a.shape[0]
    # print("shape N: ", N)
    # filter = np.array([1/3, 1/3, 1/3])
    filter = np.ones(3)/3
    b = np.convolve(a, filter, mode='same')

    total = np.empty(a.shape)

    total += a
    total += b

    print(a)
    print(b)
    print(total/2.0)


if __name__ == '__main__':
    main()